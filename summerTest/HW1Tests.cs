﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using summerWork06_08;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace summerWork06_08.Tests
{
    [TestClass()]
    public class HW1Tests
    {
        [TestMethod()]
        public void backWordsTest()
        {
            
        }

        [TestMethod()]
        public void printSumTest()
        {

        }

        [TestMethod()]
        public void sumTest()
        {
            HW1 hw1 = new HW1();
            Assert.AreEqual(6, hw1.sum(123));
            Assert.AreEqual(1, hw1.sum(1));
            Assert.AreEqual(15, hw1.sum(555));
            Assert.AreEqual(6, hw1.sum(100023));
        }
    }
}